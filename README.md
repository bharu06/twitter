# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* install ruby and rails

* bundle install

* npm install

* change Database settings in database.yml

* rake db:migrate

* start backend using rails s

* start fronend using npm start

  Frontend pings the server for every 5 min and gets the recent tweets.
server through streaming api gets the tweets and stores in the database
